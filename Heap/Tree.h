#ifndef TREE_h_
#define TREE_h_

template<class Leaf>
class Tree
{
public:
	explicit Tree(int capacity = 2)
		: elements(nullptr)
		, capacity(capacity)
		, nrOfElements(0)
	{
		if(this->capacity < 1)
			this->capacity = 2;

		elements = new Leaf*[this->capacity];
		DestroyElements();
	}

	virtual ~Tree()
	{
		Release();
	}

	void Release()
	{
		for(auto i = 0; i < nrOfElements; i++)
		{
			if(elements[i] != nullptr)
			{
				delete elements[i];
				elements[i] = nullptr;
			}
		}

		if(elements != nullptr)
		{
			delete[] elements;
			elements = nullptr;
		}
	}

	int GetSize()const
	{
		return nrOfElements;
	}

protected:
	void DestroyElements(int startIndex = 0)
	{
		if(startIndex >= this->capacity)
			return;

		for(auto i = startIndex; i < this->capacity; i++)
			elements[i] = nullptr;
	}

	void Expand(int increment = 2)
	{
		this->capacity += increment;
		Leaf** temp = new Leaf*[this->capacity];

		for(auto i = 0; i < nrOfElements; i++)
			temp[i] = elements[i];

		delete[] elements;
		elements = temp;
		DestroyElements(nrOfElements);
	}

	virtual void Insert(const Leaf& element) = 0
	{
		if(this->nrOfElements == this->capacity)
			this->Expand();
	}

	Leaf* GetRoot()const
	{
		//if(nrOfElements > 0)
			return elements[0];

		//return nullptr;
	}

	virtual int GetParentNode(int index) const = 0;
	virtual std::string PrintTree() const = 0;

	Leaf** elements;
	int capacity;
	int nrOfElements;

private:

};
#endif //TREE_h_
