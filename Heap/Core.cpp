#include <iostream>
#include <time.h>

#include <ALib/Logger.h>

#include "Heap.h"

//#define DEBUG_HEAP
static const int CAP = 1'000'000;

int main(int argc, char** argv)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	ALib::Logger::Initialize();
	ALib::Logger::SetLogLevel(ALib::LOG_LEVEL::FULL);

	clock_t t1, t2;
	auto heap = Heap<int>(CAP);

	//insert elements
	t1 = clock();
	for (auto i = 0; i < CAP; i++)
		heap.Insert(i);
	t2 = clock();

	//print tree
#ifdef DEBUG_HEAP
	ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, "Time for inserting: ", (static_cast<float>(t2) - static_cast<float>(t1)) / 100);
	ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, heap.PrintTree().c_str());
#endif // DEBUG_HEAP

	//sort elements
	std::vector<int> sortedHeap;
	sortedHeap.reserve(heap.GetSize());
	t1 = clock();
	heap.HeapSort(sortedHeap);
	t2 = clock();
	ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, "Time for sorting: ", (static_cast<float>(t2) - static_cast<float>(t1)) / 1000.0, " seconds.");

	//print sorted heap
#ifdef DEBUG_HEAP
	for(int i = 0; i < sortedHeap.size(); i++)
		ALib::Logger::Log(ALib::LOG_TYPE::NONE, sortedHeap[i], " ");
#endif // DEBUG_HEAP

	//verify
	int last = CAP;
	int amountOfErrors = 0;
	for(unsigned int i = 0; i < sortedHeap.size(); i++)
	{
		auto value = sortedHeap[i];
		if(last < value)
			amountOfErrors++;
		last = value;
	}

	ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, "Number of errors: ", amountOfErrors, " out of ", CAP, " elements!");
	ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, "Percentage wrong: ", ((amountOfErrors / (float)CAP) * 100), " %");
	std::cin.get();

	return 0;
}
