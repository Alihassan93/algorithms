#ifndef HEAP_h_
#define HEAP_h_

#include <vector>
#include <string>

#include "Tree.h"

template<class Leaf>
class Heap
	: public Tree<Leaf>
{
public:
	explicit Heap(int capacity = 2)
		: Tree<Leaf>(capacity)
	{ }

	virtual ~Heap()
	{ }

	void HeapSort(std::vector<Leaf>& heap)
	{
		for(auto i = 0; i < this->nrOfElements; ++i)
		{
			heap.emplace_back(Pop());
			--i;
		}
	}

	Leaf Pop()
	{
		Leaf* root = nullptr;
		if((root = this->GetRoot()) == nullptr)
			return NULL;

		Leaf rootElement = *root;

		delete root;
		this->elements[0] = this->elements[this->nrOfElements - 1];
		this->elements[--this->nrOfElements] = nullptr;

		PerculateDown();
		return rootElement;
	}

	void PerculateDown(int parentIndex = 0)
	{
		Leaf* parent = this->elements[parentIndex];

		auto child1Index = 2 * parentIndex + 1;
		auto child2Index = 2 * parentIndex + 2;

		if(child1Index >= this->nrOfElements || child2Index >= this->nrOfElements)
			return;

		Leaf* child1 = this->elements[child1Index];
		Leaf* child2 = this->elements[child2Index];

		if(*parent < *child1 || *parent < *child2)
		{
			if(*child1 < *child2)
			{
				Leaf* tmp = parent;
				this->elements[parentIndex] = child2;
				this->elements[child2Index] = tmp;
				return PerculateDown(child2Index);
			}

			Leaf* tmp = parent;
			this->elements[parentIndex] = child1;
			this->elements[child1Index] = tmp;
			return PerculateDown(child1Index);
		}
	}

	void PerculateUp(int currentIndex)
	{
		auto parentNodeIndex = GetParentNode(currentIndex);
		if(currentIndex <= 0/* || this->nrOfElements < 1 || ((parentNodeIndex = GetParentNode(index)) == -1)*/)
			return;

		Leaf* parentNode = this->elements[parentNodeIndex];
		Leaf* currentNode = this->elements[currentIndex];
		if(*parentNode < *currentNode)
		{
			Leaf* tempNode = currentNode;
			this->elements[currentIndex] = parentNode;
			this->elements[parentNodeIndex] = tempNode;
		}

		return PerculateUp(parentNodeIndex);
	}

	void Insert(const Leaf& element) override
	{
		Tree::Insert(element);

		this->elements[this->nrOfElements++] = new Leaf(element);
		return PerculateUp(this->nrOfElements - 1);
	}

	std::string PrintTree() const override
	{
		std::string returnString = "";
		for(auto i = 0; i < this->nrOfElements; i++)
			returnString += std::to_string(*this->elements[i]) + " ";

		return returnString;
	}

private:
	int GetParentNode(int index) const override
	{
		//if(index > 0)
			return static_cast<int>((index - 1) * 0.5f);

		//return -1;
	};
};
#endif //HEAP_h_
