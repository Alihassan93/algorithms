# README #

### What is this repository for? ###

* Consist of different projects for algorithms such as A-Star pathfinding, heap and heapsort, etc.
* Version
0.2

### How do I get set up? ###

* Configuration
Windows x64
* Deployment instructions
Depending on what project is compiled and executed, content files need to be added or moved with installation (for ex. maps when path finding).