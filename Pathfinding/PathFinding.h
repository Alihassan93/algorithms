#ifndef PATH_FINDING_h
#define PATH_FINDING_h

#include <vector>
#include <DirectXMath.h>

#include <ALib/StackAllocator.h>
#include <ALib/CustomNew.h>

static const int NUMBER_OF_NEIGHBOURS = 8;

///Used for getting valid moves
static const int NEIGHBOURS_INDICES[NUMBER_OF_NEIGHBOURS][NUMBER_OF_NEIGHBOURS] =
{
	{-1, 0}, //left
	{-1, 1}, //left-up

	{0, 1}, //up
	{1, 1}, //up-right

	{1, 0}, //right
	{1, -1}, //right-down

	{0, -1}, //down
	{-1, -1} //down-left
};

class PathFinding
{
private:
	struct Node
	{
		Node::Node(const DirectX::XMINT2& position, Node* neighbours[NUMBER_OF_NEIGHBOURS], Node* parent, int f)
		{
			this->position = position;

			for(int i = 0; i < NUMBER_OF_NEIGHBOURS; i++)
				this->neighbours[i] = neighbours[i];

			this->parent = parent;
			this->f = f;
		}

		Node::Node(const DirectX::XMINT2& position)
		{
			this->position = position;
			this->ClearNode();			
		}

		void ClearNode()
		{
			for(int i = 0; i < NUMBER_OF_NEIGHBOURS; i++)
				neighbours[i] = nullptr;

			f = 0;
			parent = nullptr;
		}

		DirectX::XMINT2 position;					//8 bytes
		Node* neighbours[NUMBER_OF_NEIGHBOURS];		//32 bytes

		Node* parent;								//4 bytes
		int f;										//4 bytes

		int padding[4];								//16 bytes (padding due to using stack allocator with placement new (power of 2, memory gotten from heap) instead of normal heap allocation)
	};												//Total: 64 bytes

	enum NODE_TYPE
	{
		OBSTACLE_NODE = 0,
		START_NODE = 1,
		TARGET_NODE = 9,
	};

	int width;
	int height;

	std::vector<std::vector<int>> map; //maps the position of each grid element
	Node** grid;

	ALib::StackAllocator stackAllocator;

	Node* startNode;
	Node* targetNode;

	std::vector<Node*> openList;
	std::vector<Node*> closedList;

	void ClearGrid(); //deallocates all nodes
	void GenerateNodes(); //generates the nodes using map and calls SetNeighbour
	void SetNeighbours(); //using the nodes to set neighbours by parenting each node to there 4/8 neighbours (depending on if they can go diagonally)

	bool InList(Node* node, const std::vector<Node*>& inputList) const;

	//checks out of bounds
	bool IsValidNode(const DirectX::XMINT2& position) const;
	bool IsValidPosition(const DirectX::XMINT2& position, int width, int height) const; 

	//used for starting exploring the most promising node in open list
	Node* LowestValueInList(const std::vector<Node*>& inputList) const;

	bool RemoveNodeFromList(Node* currentNode, std::vector<Node*>& inputList);

	//Manhattan method for calculating the heuristic value
	int Manhattan(const DirectX::XMINT2& position1, const DirectX::XMINT2& position2) const;

	float Length(const DirectX::XMINT2& position1, const DirectX::XMINT2& position2) const; //standard length function

public:
	PathFinding();
	~PathFinding();

	///Initialize: Creates map from file given as parameter
	bool ReadMap(const std::string& filename);
	
	///Can be called after map is read and nodes are created. Re-parents all nodes so trace backing needs to be done if path has to be extracted
	void FindShortestPath();

	///Prints map for debugging purposes
	void PrintMap();

	///Call only after FindShortestPath is called, so that a trace back can be successful
	void PrintPath();

	///Clears all nodes and regenerates nodes using map info (Does not read map all over)
	void Reset();

	void SetStartNode(const DirectX::XMINT2& position);
	void SetTargetNode(const DirectX::XMINT2& position);

	///Traces back and gets next node
	Node* GetNextNode() const; //TODO: Stack elements after one trace and pop back when function is called
};

#endif //PATH_FINDING_h
