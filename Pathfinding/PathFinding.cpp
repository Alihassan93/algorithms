#include "PathFinding.h"

#include <fstream>
#include <iostream>

PathFinding::PathFinding()
	: width(0)
	, height(0)
	, grid(nullptr)
	, startNode(nullptr)
	, targetNode(nullptr)
{ }

PathFinding::~PathFinding()
{
#ifndef CUSTOM_ALLOCATION
	if(grid != nullptr)
	{
		for(int i = 0; i < width * height; i++)
		{
			if(grid[i] != nullptr)
			{
				delete grid[i];
				grid[i] = nullptr;
			}
		}

		delete[] grid;
		grid = nullptr;
	}
#endif
}

#pragma region Read map
bool PathFinding::ReadMap(const std::string& filename)
{
	std::ifstream file(filename);
	if(file.is_open())
	{
		//read map
		std::string nameOfVar = "";

		file >> nameOfVar >> width;
		file >> nameOfVar >> height;

		map.resize(height);
		for(int i = 0; i < height; i++)
			map[i].resize(width);

		//store map
		int type = 0;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				file >> type;
				map[y][x] = type;
			}
		}
	}
	else
	{
		ALib::Logger::LogLine(ALib::LOG_TYPE::WARNING, "Error reading map");
		return false;
	}

	stackAllocator.Init(width * height * sizeof(Node) * 2, 64);

	GenerateNodes();

	return true;
}
#pragma endregion
#pragma region Shortest path
void PathFinding::FindShortestPath()
{
	Node* currentNode = startNode;
	openList.emplace_back(currentNode);

	while(currentNode != nullptr)
	{
		if(currentNode == targetNode) //found goal node -> stop searching
			break;

		for(int i = 0; i < NUMBER_OF_NEIGHBOURS && currentNode->neighbours[i] != nullptr; i++)
		{
			bool inClosed = InList(currentNode->neighbours[i], closedList);

			if(!inClosed)
			{
				//calculate movement cost: the movement cost from node that we came from to the node that we are going to + the movement cost from start node to current node
				int movementValue = static_cast<int>((Length(currentNode->position, startNode->position) * 10.0f)) + static_cast<int>((Length(currentNode->position, currentNode->neighbours[i]->position) * 10.0f));
				//int movementValue = static_cast<int>((currentNode->position.Length(startNode->position) * 10.0f) + (currentNode->position.Length(currentNode->neighbours[i]->position) * 10.0f));

				//calculate heuristic cost
				//int heuristicValue = static_cast<int>((currentNode->neighbours[i]->position.Length(targetNode->position) * 10.0f)); //doesn't seem to return a good result
				int heuristicValue = Manhattan(currentNode->neighbours[i]->position, targetNode->position);

				//set total value (f-value = g + h) for node
				int totalValue = currentNode->f + movementValue + heuristicValue;

				if(currentNode->neighbours[i]->f <= 0 || totalValue < currentNode->neighbours[i]->f)
				{
					//set total value (f-value = g + h) for node
					currentNode->neighbours[i]->f = totalValue;

					//parenting
					currentNode->neighbours[i]->parent = currentNode;
				}

				//add element if not in open list
				if(!InList(currentNode->neighbours[i], openList))
				{
					//insert neighbour to open list
					openList.emplace_back(currentNode->neighbours[i]);
				}
			}
		}

		//set current node to closed list and remove from open list
		closedList.emplace_back(currentNode);
		RemoveNodeFromList(currentNode, openList);

		//set current node to one of the nodes in open list with lowest f value
		currentNode = LowestValueInList(openList);
	}
}
#pragma endregion

#pragma region Print map
// only for debugging
void PathFinding::PrintMap()
{
	for(int y = 0; y < height; y++)
	{
		for(int x = 0; x < width; x++)
		{
			int value = map[y][x];
			std::cout << value << " ";
		}

		std::cout << std::endl;
	}
}

// only for debugging
void PathFinding::PrintPath()
{
	HANDLE currentConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(currentConsole, &csbi);

	for(int y = 0; y < height; y++)
	{
		for(int x = 0; x < width; x++)
		{
			bool partOfPath = false;

			Node* currentNode = targetNode;
			while(currentNode != startNode)
			{
				if(currentNode != nullptr)
				{
					if(currentNode->position.x == x && currentNode->position.y == y)
					{
						if(currentNode != targetNode)
							partOfPath = true;
						break;
					}
				}
				else
				{
					ALib::Logger::LogLine(ALib::LOG_TYPE::WARNING, "Cannot print path");
					return;
				}

				currentNode = currentNode->parent;
			}

			if(partOfPath)
			{
				SetConsoleTextAttribute(currentConsole, 10); //light green color
				std::cout << "# ";
			}
			else
			{
				int value = map[y][x];

				if(value == NODE_TYPE::OBSTACLE_NODE)
					SetConsoleTextAttribute(currentConsole, 12); //red color
				else if(value == START_NODE)
					SetConsoleTextAttribute(currentConsole, 2); //dark green color
				else if(value == TARGET_NODE)
					SetConsoleTextAttribute(currentConsole, 2); //dark green color
				else
					SetConsoleTextAttribute(currentConsole, csbi.wAttributes);

				std::cout << value << " ";
			}
		}

		std::cout << std::endl;
	}

	SetConsoleTextAttribute(currentConsole, csbi.wAttributes); //reset color
}
#pragma endregion

void PathFinding::ClearGrid()
{
	for(int y = 0; y < height; y++)
	{
		for(int x = 0; x < width; x++)
		{
			if(grid[y * width + x] != nullptr)
				grid[y * width + x]->ClearNode();
		}
	}

	stackAllocator.Free();
	grid = nullptr;

	openList.clear();
	closedList.clear();
}

#pragma region Generating nodes
void PathFinding::GenerateNodes()
{
	if(grid == nullptr)
	{
		grid = NEW(sizeof(Node) * width * height, stackAllocator) Node*[height * width];
		for(int i = 0; i < height * width; i++)
			grid[i] = nullptr;
	}
	else
	{
		ALib::Logger::LogLine(ALib::LOG_TYPE::WARNING, "Grid already allocated");
		return;
	}

	for(int y = 0; y < height; y++)
	{
		for(int x = 0; x < width; x++)
		{
			Node* node = nullptr;
			if(map[y][x] != NODE_TYPE::OBSTACLE_NODE)
			{
				//allocate node and set to grid for all nodes except obstacles
				node = NEW(sizeof(Node), stackAllocator) Node(DirectX::XMINT2(x, y));
				grid[y * width + x] = node;
			}

			switch(map[y][x])
			{
			case NODE_TYPE::START_NODE:
				startNode = node;
				break;

			case NODE_TYPE::TARGET_NODE:
				targetNode = node;
				break;
			}
		}
	}

	if(startNode == nullptr || targetNode == nullptr)
	{
		ALib::Logger::LogLine(ALib::LOG_TYPE::WARNING, "Start node or target node are not set");
		return;
	}

	SetNeighbours();
}
#pragma endregion
#pragma region Setting neighbours
void PathFinding::SetNeighbours()
{
	int validNeighbours = 0;
	Node* currentNode = nullptr;

	for(int y = 0; y < height; y++)
	{
		for(int x = 0; x < width; x++)
		{
			currentNode = grid[y * width + x];
			if(currentNode != nullptr) //is not an obstacle
			{
				for(int i = 0; i < NUMBER_OF_NEIGHBOURS; i++)
				{
					DirectX::XMINT2 position = currentNode->position;

					DirectX::XMINT2 tmpPosition = DirectX::XMINT2(position.x + NEIGHBOURS_INDICES[i][0], position.y + NEIGHBOURS_INDICES[i][1]);
					if(IsValidPosition(tmpPosition, width, height))
					{
						if(IsValidNode(tmpPosition))
							currentNode->neighbours[validNeighbours++] = grid[tmpPosition.y * width + tmpPosition.x];
					}
					else
					{
						ALib::Logger::LogLine(ALib::LOG_TYPE::WARNING, "Not a valid position");
						return;
					}
				}

				validNeighbours = 0;
			}
		}
	}
}
#pragma endregion

#pragma region Helper functions
bool PathFinding::InList(Node* node, const std::vector<Node*>& inputList) const
{
	for(Node* n : inputList)
		if(n == node)
			return true;

	return false;
}

bool PathFinding::IsValidNode(const DirectX::XMINT2& position) const
{
	if((position.y * width + position.x) > width * height)
	{
		ALib::Logger::LogLine(ALib::LOG_TYPE::WARNING, "Position not valid");
		return false;
	}

	return (grid[position.y * width + position.x] != nullptr ? true : false);
}

bool PathFinding::IsValidPosition(const DirectX::XMINT2& position, int width, int height) const
{
	return position.x >= 0 && position.x < width && position.y >= 0 && position.y < height;
}

PathFinding::Node* PathFinding::LowestValueInList(const std::vector<Node*>& inputList) const
{
	if(inputList.empty())
		return nullptr;

	int lowestValue = inputList[0]->f;
	int index = 0;

	for(unsigned int i = 1; i < inputList.size(); i++)
	{
		if(inputList[i]->f < lowestValue)
		{
			lowestValue = inputList[i]->f;
			index = i;
		}
	}

	return inputList[index];
}

bool PathFinding::RemoveNodeFromList(Node* currentNode, std::vector<Node*>& inputList)
{
	bool foundElement = false;
	for(unsigned int i = 0; i < inputList.size() && !foundElement; i++)
	{
		if(inputList[i] == currentNode)
		{
			inputList.erase(inputList.begin() + i);
			foundElement = true;
		}
	}

	if(!foundElement)
		ALib::Logger::LogLine(ALib::LOG_TYPE::WARNING, "Error finding element");

	return foundElement;
}

int PathFinding::Manhattan(const DirectX::XMINT2& position1, const DirectX::XMINT2& position2) const
{
	return (abs(position1.x - position2.x) + abs(position1.y - position2.y));
}

float PathFinding::Length(const DirectX::XMINT2& position1, const DirectX::XMINT2& position2) const
{
	return static_cast<float>(sqrt(pow(position1.x - position2.x, 2.0) + pow(position1.y - position2.y, 2.0)));
}
#pragma endregion

void PathFinding::Reset()
{
	ClearGrid();
	GenerateNodes();
}

void PathFinding::SetStartNode(const DirectX::XMINT2& position)
{
	if((position.y * width + position.x) < width * height)
		startNode = grid[position.y * width + position.x];
}

void PathFinding::SetTargetNode(const DirectX::XMINT2& position)
{
	if((position.y * width + position.x) < width * height)
		targetNode = grid[position.y * width + position.x];
}

PathFinding::Node* PathFinding::GetNextNode() const
{
	int counter = 0;
	Node* currentNode = targetNode;
	while(currentNode != startNode)
	{
		if(currentNode != nullptr)
		{
			if(currentNode->parent == startNode)
				return currentNode;
		}
		else
		{
			ALib::Logger::LogLine(ALib::LOG_TYPE::WARNING, "Cannot find start node");
			return nullptr;
		}

		currentNode = currentNode->parent;
		counter++;
	}

	return nullptr;
}
