#include <iostream>
#include <chrono>
#include <string>

#include "PathFinding.h"

PathFinding path;
float changePosTimer = 0.0f;
static const float DEAD_LINE = 0.2f;

std::chrono::high_resolution_clock::time_point currentTime;
std::chrono::high_resolution_clock::time_point previousTime;
std::chrono::high_resolution_clock::duration deltaTime;

void Update(std::chrono::high_resolution_clock::duration deltaTime);

int main(int argc, char** argv)
{
	_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF);
	ALib::Logger::Initialize();
	ALib::Logger::SetLogLevels(ALib::LOG_LEVEL::ONLY_DEBUG | ALib::LOG_LEVEL::ONLY_CRITICAL);

	currentTime = std::chrono::high_resolution_clock::now();
	previousTime = currentTime;

	path.ReadMap("map1.map");
	path.FindShortestPath();
	path.PrintPath();

	//////////////////////////////////////////////////////////////////////////
	// Testing
	//////////////////////////////////////////////////////////////////////////
	//std::string test = "THIS IS A STRING";
	//ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, test);
	//ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, test, "test");
	//ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, 3, " TEST");
	//ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, "TEST ", 1);
	//ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, 1);
	//ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, 1, " TEST ");
	//ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, 1, 2, 3, " Test ", 2, 5, 6);
	//ALib::Logger::LogLine(ALib::LOG_TYPE::INFO, 1, ", ", test.c_str(), ", ", test.c_str(), " ", 2, 3, 4, 5, 6);

	//////////////////////////////////////////////////////////////////////////
	// Update loop
	//////////////////////////////////////////////////////////////////////////
	//while(true)
	//{
	//	currentTime = std::chrono::high_resolution_clock::now();
	//	deltaTime = currentTime - previousTime;
	//	previousTime = currentTime;
	//
	//	Update(deltaTime);
	//}

	std::cin.get();
	return 0;
}

void Update(std::chrono::high_resolution_clock::duration deltaTime)
{
	changePosTimer += deltaTime.count()*1.e-9f;
	if(changePosTimer >= DEAD_LINE)
	{
		path.FindShortestPath();
		if(path.GetNextNode() != nullptr)
		{
			DirectX::XMINT2 nextPosition = path.GetNextNode()->position;
#ifdef _WIN32
			system("clear");
#endif
			path.PrintPath();
			path.Reset();
			path.SetStartNode(nextPosition);
		}

		changePosTimer = 0.0f;
	}
}
