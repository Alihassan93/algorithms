import math
import random
from operator import mul

# Training set
inputValues = [[0,0,1],
               [0,1,1],
               [1,0,1],
               [1,1,1]] # (4, 3)

outputValues = [[0],
                [0],
                [1],
                [1]]    # (4, 1)

# Global values
NR_LAYERS = 2

# Define dot product
dot = lambda X, Y: sum(map(lambda x, y: x * y, X, Y))

# Sigmoid and its derivative - Derivative used for backpropagation whilst sigmoid for forward propagation
sigmoid = lambda x: 1 / (1 + math.exp(-x)) # TODO: Represent this better if needed
derivativeOfSigmoid = lambda x: x * (1-x) # TODO: Represent this better if needed

class Neuron:
    val = []

class Layer:
    def __init__(self, neuronAmount):
        self.neurons = []
        self.weights = []
        
        for i in range(neuronAmount):
            self.neurons.extend([Neuron()])

    def debug(self):
        for i in range(len(self.weights)):
            print("Weight " + str(i) + ": " + str(self.weights[i]))

class Network:
    def __init__(self, layersAmount):
        self.layers = []
        
        for i in range(layersAmount):
            self.layers.extend([Layer(4)]) # Creates layer with x amount of neurons

    def setTrainData(self, inputNeurons, outputNeurons):
        # Set input neurons in first layer        
        if len(self.layers[0].neurons) == len(inputNeurons):
            for i in range(len(inputNeurons)):
                self.layers[0].neurons[i].val = inputNeurons[i]

        # TODO: Output neurons should not be applied, since this will be checked for when calculating the error
        # Set output neurons in last layer
        #lastLayerIndex = len(self.layers) - 1
        #if len(self.layers[lastLayerIndex].neurons) == len(outputNeurons):
        #    for i in range(len(outputNeurons)):
        #        self.layers[lastLayerIndex].neurons[i].val = outputNeurons[i]

        # Create weights of the right dimension
        for i in range(len(self.layers[0].neurons[0].val)):
            self.layers[1].weights.extend([int()]) # TODO: Change this so that weights are set for all except input and out layers

    def trainNetwork(self):
        # Initialize weights (Need to be of dimension (3, 1) due to it is going to be mul. with input
        for l in range(1, len(self.layers)): # Start at index due to input layer not having weights
            for w in range(len(self.layers[l].weights)):
                self.layers[l].weights[w] = random.random() - 1
        self.debugWeights()
        
        # Forward propogate (Use sigmoid)

        # Calculate error(Use derivative)

        # Update weights (Back propagation)

    def debugWeights(self):
        for i in range(len(self.layers)):
            print("Layer " + str(i) + ": ")
            self.layers[i].debug()
            print("\n")
            
    def debug(self):
        for l in range(len(self.layers) ):
            for n in range(len(self.layers[l].neurons)):
                print("Layer: " + str(l) + " Neuron: " + str(n) + " = " + str(self.layers[l].neurons[n].val))
            print("\n")
            
if __name__ == "__main__": # Main method
    random.seed(1)
    network = Network(NR_LAYERS)
    network.setTrainData(inputValues, outputValues)
    network.trainNetwork()
    network.debug()
    
